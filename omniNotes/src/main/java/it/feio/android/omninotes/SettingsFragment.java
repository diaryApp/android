/*******************************************************************************
 * Copyright 2014 Federico Iosue (federico.iosue@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package it.feio.android.omninotes;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.*;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import it.feio.android.analitica.AnalyticsHelper;
import it.feio.android.omninotes.async.DataBackupIntentService;
import it.feio.android.omninotes.helpers.PermissionsHelper;
import it.feio.android.omninotes.models.ONStyle;
import it.feio.android.omninotes.utils.*;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class SettingsFragment extends PreferenceFragment {

	private SharedPreferences prefs;

	private final int SPRINGPAD_IMPORT = 0;
	private final int RINGTONE_REQUEST_CODE = 100;
	public final static String XML_NAME = "xmlName";
	private Activity activity;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int xmlId = R.xml.settings;
		if (getArguments() != null && getArguments().containsKey(XML_NAME)) {
			xmlId = ResourcesUtils.getXmlId(OmniNotes.getAppContext(), ResourcesUtils.ResourceIdentifiers.xml, String
					.valueOf(getArguments().get(XML_NAME)));
		}
		addPreferencesFromResource(xmlId);
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
		prefs = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_MULTI_PROCESS);
		setTitle();
	}


	private void setTitle() {
		String title = getString(R.string.settings);
		if (getArguments() != null && getArguments().containsKey(XML_NAME)) {
			String xmlName = getArguments().getString(XML_NAME);
			if (!TextUtils.isEmpty(xmlName)) {
				int stringResourceId = getActivity().getResources().getIdentifier(xmlName.replace("settings_",
						"settings_screen_"), "string", getActivity().getPackageName());
				title = stringResourceId != 0 ? getString(stringResourceId) : title;
			}
		}
		Toolbar toolbar = ((Toolbar) getActivity().findViewById(R.id.toolbar));
		if (toolbar != null) toolbar.setTitle(title);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				getActivity().onBackPressed();
				break;
			default:
				Log.e(Constants.TAG, "Wrong element choosen: " + item.getItemId());
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
		super.onPreferenceTreeClick(preferenceScreen, preference);
		if (preference instanceof PreferenceScreen) {
			((SettingsActivity) getActivity()).switchToScreen(preference.getKey());
		}
		return false;
	}


	@SuppressWarnings("deprecation")
	@Override
	public void onResume() {
		super.onResume();

		// Export notes
		Preference export = findPreference("settings_export_data");
		if (export != null) {
			export.setOnPreferenceClickListener(arg0 -> {

				// Inflate layout
				LayoutInflater inflater = getActivity().getLayoutInflater();
				View v = inflater.inflate(R.layout.dialog_backup_layout, null);

				// Finds actually saved backups names
				PermissionsHelper.requestPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, R
						.string.permission_external_storage, activity.findViewById(R.id.crouton_handle), () -> export
						(v));

				return false;
			});
		}

		// Import notes
		Preference importData = findPreference("settings_import_data");
		if (importData != null) {
			importData.setOnPreferenceClickListener(arg0 -> {
				importNotes();
				return false;
			});
		}

		// Settings reset
		Preference resetData = findPreference("reset_all_data");
		if (resetData != null) {
			resetData.setOnPreferenceClickListener(arg0 -> {

				new MaterialDialog.Builder(activity)
						.content(R.string.reset_all_data_confirmation)
						.positiveText(R.string.confirm)
						.callback(new MaterialDialog.ButtonCallback() {
							@Override
							public void onPositive(MaterialDialog dialog) {
								prefs.edit().clear().commit();
								File db = getActivity().getDatabasePath(Constants.DATABASE_NAME);
								StorageHelper.delete(getActivity(), db.getAbsolutePath());
								File attachmentsDir = StorageHelper.getAttachmentDir(getActivity());
								StorageHelper.delete(getActivity(), attachmentsDir.getAbsolutePath());
								File cacheDir = StorageHelper.getCacheDir(getActivity());
								StorageHelper.delete(getActivity(), cacheDir.getAbsolutePath());
								SystemHelper.restartApp(getActivity().getApplicationContext(), MainActivity.class);
							}
						})
						.build().show();

				return false;
			});
		}

	}


	private void importNotes() {
		final CharSequence[] backups = StorageHelper.getExternalStoragePublicDir().list();

		if (backups.length == 0) {
			((SettingsActivity)getActivity()).showMessage(R.string.no_backups_available, ONStyle.WARN);
		} else {

			MaterialDialog importDialog = new MaterialDialog.Builder(getActivity())
					.title(R.string.data_import_message)
					.items(backups)
					.positiveText(R.string.confirm)
					.callback(new MaterialDialog.ButtonCallback() {
						@Override
						public void onPositive(MaterialDialog materialDialog) {

						}
					}).build();

			// OnShow is overridden to allow long-click on item so user can remove them
			importDialog.setOnShowListener(dialog -> {

				ListView lv = importDialog.getListView();
				assert lv != null;
				lv.setOnItemClickListener((parent, view, position, id) -> {

					// Retrieves backup size
					File backupDir = StorageHelper.getBackupDir(backups[position].toString());
					long size = StorageHelper.getSize(backupDir) / 1024;
					String sizeString = size > 1024 ? size / 1024 + "Mb" : size + "Kb";

					// Check preference presence
					String prefName = StorageHelper.getSharedPreferencesFile(getActivity()).getName();
					boolean hasPreferences = (new File(backupDir, prefName)).exists();

					String message = backups[position]
							+ " (" + sizeString
							+ (hasPreferences ? " " + getString(R.string.settings_included) : "")
							+ ")";

					new MaterialDialog.Builder(getActivity())
							.title(R.string.confirm_restoring_backup)
							.content(message)
							.positiveText(R.string.confirm)
							.callback(new MaterialDialog.ButtonCallback() {
								@Override
								public void onPositive(MaterialDialog materialDialog) {

									//((OmniNotes)getActivity().getApplication()).getAnalyticsHelper().trackEvent(AnalyticsHelper.CATEGORIES.SETTING,
									//		"settings_import_data");

									importDialog.dismiss();

									// An IntentService will be launched to accomplish the import task
									Intent service = new Intent(getActivity(),
											DataBackupIntentService.class);
									service.setAction(DataBackupIntentService.ACTION_DATA_IMPORT);
									service.putExtra(DataBackupIntentService.INTENT_BACKUP_NAME,
											backups[position]);
									getActivity().startService(service);
								}
							}).build().show();
				});

				// Creation of backup removal dialog
				lv.setOnItemLongClickListener((parent, view, position, id) -> {

					// Retrieves backup size
					File backupDir = StorageHelper.getBackupDir(backups[position].toString());
					long size = StorageHelper.getSize(backupDir) / 1024;
					String sizeString = size > 1024 ? size / 1024 + "Mb" : size + "Kb";

					new MaterialDialog.Builder(getActivity())
							.title(R.string.confirm_removing_backup)
							.content(backups[position] + "" + " (" + sizeString + ")")
							.positiveText(R.string.confirm)
							.callback(new MaterialDialog.ButtonCallback() {
								@Override
								public void onPositive(MaterialDialog materialDialog) {
									importDialog.dismiss();
									// An IntentService will be launched to accomplish the deletion task
									Intent service = new Intent(getActivity(),
											DataBackupIntentService.class);
									service.setAction(DataBackupIntentService.ACTION_DATA_DELETE);
									service.putExtra(DataBackupIntentService.INTENT_BACKUP_NAME,
											backups[position]);
									getActivity().startService(service);
								}
							}).build().show();

					return true;
				});
			});

			importDialog.show();
		}
	}


	private void export(View v) {
		final List<String> backups = Arrays.asList(StorageHelper.getExternalStoragePublicDir().list());

		// Sets default export file name
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_EXPORT);
		String fileName = sdf.format(Calendar.getInstance().getTime());
		final EditText fileNameEditText = (EditText) v.findViewById(R.id.export_file_name);
		final TextView backupExistingTextView = (TextView) v.findViewById(R.id.backup_existing);
		fileNameEditText.setHint(fileName);
		fileNameEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
			@Override
			public void afterTextChanged(Editable arg0) {
				if (backups.contains(arg0.toString())) {
					backupExistingTextView.setText(R.string.backup_existing);
				} else {
					backupExistingTextView.setText("");
				}
			}
		});

		new MaterialDialog.Builder(getActivity())
				.title(R.string.data_export_message)
				.customView(v, false)
				.positiveText(R.string.confirm)
				.callback(new MaterialDialog.ButtonCallback() {
					@Override
					public void onPositive(MaterialDialog materialDialog) {
						//((OmniNotes)getActivity().getApplication()).getAnalyticsHelper().trackEvent(AnalyticsHelper.CATEGORIES.SETTING, "settings_export_data");
						// An IntentService will be launched to accomplish the export task
						Intent service = new Intent(getActivity(), DataBackupIntentService.class);
						service.setAction(DataBackupIntentService.ACTION_DATA_EXPORT);
						String backupName = StringUtils.isEmpty(fileNameEditText.getText().toString()) ?
								fileNameEditText.getHint().toString() : fileNameEditText.getText().toString();
						service.putExtra(DataBackupIntentService.INTENT_BACKUP_NAME, backupName);
						getActivity().startService(service);
					}
				}).build().show();
	}


	@Override
	public void onStart() {
		super.onStart();
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case SPRINGPAD_IMPORT:
					Uri filesUri = intent.getData();
					String path = FileHelper.getPath(getActivity(), filesUri);
					// An IntentService will be launched to accomplish the import task
					Intent service = new Intent(getActivity(), DataBackupIntentService.class);
					service.setAction(DataBackupIntentService.ACTION_DATA_IMPORT_SPRINGPAD);
					service.putExtra(DataBackupIntentService.EXTRA_SPRINGPAD_BACKUP, path);
					getActivity().startService(service);
					break;

				case RINGTONE_REQUEST_CODE:
					Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
					String notificationSound = uri == null ? null : uri.toString();
					prefs.edit().putString("settings_notification_ringtone", notificationSound).apply();
					break;

				default:
					Log.e(Constants.TAG, "Wrong element choosen: " + requestCode);
			}
		}
	}
}
