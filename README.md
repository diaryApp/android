Diary is a friend you can share all your secrets, feelings with. 

##Features
* Basic add, modify, trash and delete notes actions
* Image attachments
* Manage your notes using categories
* To-do list
* Notes shortcut on home screen
* Export/import notes to backup
